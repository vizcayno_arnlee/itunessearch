//
//  SearchHistoryManager.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "SearchHistoryManager.h"

@implementation SearchHistoryManager

#pragma mark - Public Methods

+ (NSArray *)getSearchHistoryArray
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if ([userDefaults objectForKey:kSearchHistory]) {
        return (NSArray *)[userDefaults objectForKey:kSearchHistory];
    }

    return [NSArray array];
}

+ (void)addSearchHistory:(NSString *)searchText
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *recentSearchArray = [[NSMutableArray alloc] init];
    
    if ([userDefaults objectForKey:kSearchHistory])
        [recentSearchArray addObjectsFromArray:(NSArray *)[userDefaults objectForKey:kSearchHistory]];
    
    if (searchText && [searchText length] > 0 && ![recentSearchArray containsObject:searchText])
        [recentSearchArray insertObject:searchText atIndex:0];
    
    if ([recentSearchArray count] > 6)
        [recentSearchArray removeLastObject];
    
    [userDefaults setObject:recentSearchArray forKey:kSearchHistory];
    [userDefaults synchronize];
}

+ (NSArray *)getVisitedHistoryArray
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults objectForKey:kVisitedHistory]) {
        return (NSArray *)[userDefaults objectForKey:kVisitedHistory];
    }
    
    return [NSArray array];
}

+ (void)addVisitedHistory:(NSDictionary *)trackInformation
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *recentVisitedArray = [[NSMutableArray alloc] init];
    
    if ([userDefaults objectForKey:kVisitedHistory])
        [recentVisitedArray addObjectsFromArray:(NSArray *)[userDefaults objectForKey:kVisitedHistory]];
    
    if (trackInformation && [trackInformation count] > 0 && ![recentVisitedArray containsObject:trackInformation])
        [recentVisitedArray insertObject:trackInformation atIndex:0];
    
    if ([recentVisitedArray count] > 10)
        [recentVisitedArray removeLastObject];
    
    [userDefaults setObject:recentVisitedArray forKey:kVisitedHistory];
    [userDefaults synchronize];
}

@end
