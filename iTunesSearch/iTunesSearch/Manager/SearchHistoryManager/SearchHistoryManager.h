//
//  SearchHistoryManager.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSearchHistory @"SearchHistory"
#define kVisitedHistory @"VisitedHistory"

NS_ASSUME_NONNULL_BEGIN

@interface SearchHistoryManager : NSObject

#pragma mark - Public Methods

// Method to get search history array from user defaults.
// @return NSArray, search history array.
+ (NSArray *)getSearchHistoryArray;

// Method to add to search history array from user defaults.
// @param searchText: NSString, search text to be added
+ (void)addSearchHistory:(NSString *)searchText;

// Method to get visited track history array from user defaults.
// @return NSArray, visited track history array.
+ (NSArray *)getVisitedHistoryArray;

// Method to add to visited track history array from user defaults.
// @param trackInformation: NSDictionary, track to be added
+ (void)addVisitedHistory:(NSDictionary *)trackInformation;

@end

NS_ASSUME_NONNULL_END
