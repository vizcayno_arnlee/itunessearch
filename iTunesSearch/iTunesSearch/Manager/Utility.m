//
//  Utility.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "AppDelegate.h"
#import "Utility.h"

@implementation Utility

#pragma mark - Public Methods

+ (void)showActivityIndicator {
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    UIView *coverView = [[UIView alloc] initWithFrame:appDelegate.window.frame];
    [coverView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.4]];
    [coverView setTag:kActivityIndicatorTag];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(coverView.frame.size.width/2, coverView.frame.size.height/2);
    [spinner setHidesWhenStopped:YES];
    [spinner startAnimating];
    
    [coverView addSubview:spinner];
    [appDelegate.window addSubview:coverView];
}

+ (void)hideActivityIndicator {
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    for (UIView *view in [appDelegate.window subviews])
    {
        if ([view tag] == kActivityIndicatorTag) {
            [view removeFromSuperview];
        }
    }
}

+ (void)showAlertTitle:(NSString *)title withMessage:(NSString *)message viewController:(UIViewController *)viewController {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alert addAction:defaultAction];
    
    [viewController presentViewController:alert animated:YES completion:nil];
}

@end
