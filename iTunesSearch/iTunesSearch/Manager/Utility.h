//
//  Utility.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kActivityIndicatorTag 19890901
#define kError @"Error"

NS_ASSUME_NONNULL_BEGIN

@interface Utility : NSObject

#pragma mark - Public Methods

// Method to display activity indicator
+ (void)showActivityIndicator;
// Method to remove activity indicator
+ (void)hideActivityIndicator;

// Method to display alert view
// @param title: NSString, alert view title.
// @param message: NSString, alert view title.
// @param viewController: UIViewController, view to present alert view.
+ (void)showAlertTitle:(NSString *)title withMessage:(NSString *)message viewController:(UIViewController *)viewController;

@end

NS_ASSUME_NONNULL_END
