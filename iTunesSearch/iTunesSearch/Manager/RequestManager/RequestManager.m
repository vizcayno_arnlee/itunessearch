//
//  RequestManager.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "RequestManager.h"

#import <SystemConfiguration/SystemConfiguration.h>

#import <netinet/in.h>
#import <netinet6/in6.h>

@implementation RequestManager

static RequestManager *_sharedManager;

+ (RequestManager *)sharedManager {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _requestController = [[RequestController alloc] init];
    }
    return self;
}

- (void)setServerURL:(NSString *)serverURL
{
    [_requestController setServerURL:serverURL];
}

- (NSString *)getServerURL
{
    return [_requestController getServerURL];
}

- (BOOL)isServerURLEmpty
{
    return [_requestController isServerURLEmpty];
}

- (void)postRequestWithAction:(NSString *)action
                   parameters:(NSDictionary *)parameters
                   completion:(void (^)(id data, NSURLResponse *response, NSError *error))completionHandler
                      failure:(void (^)(id data, NSURLResponse *response, NSError *error))failureHandler
{
    //Turn on for debbuging
    //NSLog(@"RequestManager: postRequestWithAction with parameters");
    
    // Check internet connection
    if (![RequestManager isInternetReachable]) {
        failureHandler(@"No internet connection.", nil, [NSError errorWithDomain:@"" code:400 userInfo:@{NSLocalizedDescriptionKey: @"No internet connection."}]);
    }
    
    // return if server url is empty
    if ([self isServerURLEmpty]) {
        //Turn on for debbuging
        //NSLog(@"Server URL is empty.");
        return;
    }
    
    // create API POST request
    [_requestController postRequestWithAction:action parameters:parameters completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        //Turn on for debbuging
        //NSLog(@"Response: %@", response);
        
        // parse data to json
        id json = data?[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]:nil;
        [self replaceNSNullWithEmptyString:json];
        
        //Turn on for debbuging
        //NSLog(@"JSON: %@", json);
        
        //Turn on for debbuging
        //NSLog(@"Error: %@", error.description);
        
        if (error || ![RequestManager isStatusCodeAcceptable:response]) {
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               // call block method for error if not nil
                               if (failureHandler != nil) {
                                   failureHandler(json, response, error);
                               }
                           });
            
            return;
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (completionHandler != nil)
                {
                    // check json data object type and call block method for completion
                    if ([json isKindOfClass:[NSDictionary class]])
                    {
                        completionHandler([NSDictionary dictionaryWithDictionary:json], response, error);
                    }
                    else if ([json isKindOfClass:[NSArray class]])
                    {
                        completionHandler([NSArray arrayWithArray:json], response, error);
                    }
                    else
                    {
                        completionHandler(json, response, error);
                    }
                }
                
            });
        }
    }];
}

- (NSString *)errorMessageFromRequest:(id)data NSURLResponse:(NSURLResponse *)errorResponse
{    
    NSString *msg = @"Server Error";
    
    if ([data isKindOfClass:[NSDictionary class]])
    {
        for (NSString *key in (NSDictionary *)data) {
            
            id value = [data objectForKey:key];
            
            if ([value isKindOfClass:[NSArray class]]) {
                msg = [(NSArray *)value objectAtIndex:0]?[(NSArray *)value objectAtIndex:0]:msg;
            }
            else if ([value isKindOfClass:[NSString class]]) {
                msg = (NSString *)value;
            }
            else
            {
                msg = [msg stringByAppendingFormat:@" (%ld)", (long)[(NSHTTPURLResponse *)errorResponse statusCode]];
            }
            
            break;
        }
    }
    else if ([data isKindOfClass:[NSArray class]])
    {
        msg = [(NSArray *)data objectAtIndex:0]?[(NSArray *)data objectAtIndex:0]:msg;
    }
    else if ([data isKindOfClass:[NSString class]])
    {
        msg = (NSString *)data;
    }
    else
    {
        msg = [msg stringByAppendingFormat:@" (%ld)", (long)[(NSHTTPURLResponse *)errorResponse statusCode]];
    }
    
    return msg;
}

#pragma mark - Private Methods

- (void)replaceNSNullWithEmptyString:(id)object
{
    if ([object isKindOfClass:[NSDictionary class]])
    {
        for (NSString *key in [object allKeys]) {
            if ([object[key] isEqual:[NSNull null]]) {
                object[key] = nil;
            } else {
                [self replaceNSNullWithEmptyString:object[key]];
            }
        }
    }
    else if ([object isKindOfClass:[NSArray class]])
    {
        for (id obj in object) {
            [self replaceNSNullWithEmptyString:obj];
        }
    }
}

#pragma mark - Public Methods

+ (NSIndexSet *)acceptableStatusCodes
{
    return [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 99)];
}

+ (BOOL)isStatusCodeAcceptable:(NSURLResponse *)response
{
    if ([[self acceptableStatusCodes] containsIndex:[(NSHTTPURLResponse *)response statusCode]])
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isInternetReachable
{
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    if(reachability == NULL)
        return false;
    
    if (!(SCNetworkReachabilityGetFlags(reachability, &flags))) {
        CFBridgingRelease(reachability);
        return false;
    } else
        CFBridgingRelease(reachability);
    
    if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
        // if target host is not reachable
        return false;
    
    BOOL isReachable = false;
    
    if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
    {
        // if target host is reachable and no connection is required
        // then we'll assume that your on Wi-Fi
        isReachable = true;
    }
    
    
    if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
         (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
    {
        // ... and the connection is on-demand (or on-traffic) if the
        //     calling application is using the CFSocketStream or higher APIs
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
        {
            // ... and no [user] intervention is needed
            isReachable = true;
        }
    }
    
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
    {
        // ... but WWAN connections are OK if the calling application
        //     is using the CFNetwork (CFSocketStream?) APIs.
        isReachable = true;
    }
    
    return isReachable;
}

@end
