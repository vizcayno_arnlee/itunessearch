//
//  RequestController.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "RequestController.h"

@implementation RequestController 

- (id)init
{
    self = [super init];
    if (self) {
        self.requestLock = [[NSRecursiveLock alloc] init];
    }
    return self;
}

- (id)initWithServerURL:(NSString *)url
{
    self = [super init];
    if (self) {
        _server = url;
    }
    return self;
}

- (void)setServerURL:(NSString *)url
{
    _server = url;
}

- (NSString *)getServerURL
{
    return _server;
}

- (BOOL)isServerURLEmpty
{
    return _server == nil && _server.length == 0 ? TRUE: FALSE;
}

- (void)postRequestWithAction:(NSString *)action
                   parameters:(NSDictionary *)parameters
                   completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler
{
    // create new API request
    NSMutableURLRequest *request = [self initializeURLRequestWithAction:action method:@"POST" parameters:parameters];
    
    // process request with completion handler
    [self processRequest:request completionHandler:completionHandler];
}

#pragma mark - Private Methods

- (NSMutableURLRequest *)initializeURLRequestWithAction:(NSString *)action method:(NSString *)method parameters:(NSDictionary *)parameters
{
    NSURL *actionURL = [NSURL URLWithString:[[_server stringByAppendingString:action] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
     // check action url if is a valid URL
     if ([self isValidURL:action]) {
        actionURL = [NSURL URLWithString:action];
     }
    
    // Arnlee, log api call.
    //NSLog(@"URL %@", actionURL);
    
    // initialize URLRequest
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:actionURL
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:60.0];
    
    // set http header fields
    [self setupHTTPHeaderField:request];
    
    // set method
    [request setHTTPMethod:method];
    
    // set parameters if exist
    if (parameters && [parameters count] > 0) {
        [request setHTTPBody:[self httpBodyForParamsDictionary:parameters]];
    }
    
    //request logs
    /*
    NSLog(@"header %@", [request allHTTPHeaderFields]);
    
    NSLog(@"action %@", [request URL]);
    
    NSLog(@"method %@", [request HTTPMethod]);
    
    NSLog(@"body %@", [[NSString alloc]initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    */
    return request;
}

- (BOOL)isValidURL:(NSString *)string
{
    NSUInteger length = [string length];
    
    if (length > 0) {
        NSError *error = nil;
        NSDataDetector *dataDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
        if (dataDetector && !error) {
            NSRange range = NSMakeRange(0, length);
            NSRange notFoundRange = (NSRange){NSNotFound, 0};
            NSRange linkRange = [dataDetector rangeOfFirstMatchInString:string options:0 range:range];
            if (!NSEqualRanges(notFoundRange, linkRange) && NSEqualRanges(range, linkRange)) {
                return YES;
            }
        }
        else {
            NSLog(@"Could not create link data detector: %@ %@", [error localizedDescription], [error userInfo]);
        }
    }
    return NO;
}

- (void)setupHTTPHeaderField:(NSMutableURLRequest *)request
{
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"max-age=0" forHTTPHeaderField:@"Cache-Control"];
}

- (NSData *)httpBodyForParamsDictionary:(NSDictionary *)parameters
{
    NSMutableArray *parameterArray = [NSMutableArray array];
    
    // enumerate parameter http body
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", key, [self percentEscapeString:obj]];
        [parameterArray addObject:param];
    }];
    
    // set multiple parameters combined with &
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    
    // return parameters with string UTF-8 encoding
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)percentEscapeString:(NSString *)string
{
    // convert characters to allowed url character set
    NSString *result = [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
    
    return [result stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}

- (void)processRequest:(NSURLRequest *)request completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler
{
    [self.requestLock lock];
    // process request on a thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                   {
                       // process request with completion handler
                       [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:completionHandler] resume];
                   });
    [self.requestLock unlock];
}

@end
