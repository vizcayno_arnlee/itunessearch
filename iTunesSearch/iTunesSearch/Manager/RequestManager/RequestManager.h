//
//  RequestManager.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "RequestController.h"

NS_ASSUME_NONNULL_BEGIN

@class RequestController;

@interface RequestManager : NSObject

// Request Controller, an object that handle API request.
@property (nonatomic, strong, readonly) RequestController *requestController;

// Singleton class
+ (RequestManager *)sharedManager;

// Initialize class.
- (instancetype)init;

// Method to set server url.
// @param url: NSString, url to set as server url.
- (void)setServerURL:(NSString *)serverURL;
// Method to get server url.
- (NSString *)getServerURL;
// Method to check server url is empty.
- (BOOL)isServerURLEmpty;

// Method to request API with POST method.
// @param action: NSString, API action to request or execute.
// @param parameters: NSDictionary, parameters for API request.
// @param completion: A block method to be executed when API request is finished successfully or failed.
// This block returns response data in JSON(object: NSDictionary/NSArray/id) and error for any error encountered.
- (void)postRequestWithAction:(NSString *)action
                   parameters:(nullable NSDictionary *)parameters
                   completion:(nullable void (^)(id data, NSURLResponse *response, NSError *error))completionHandler
                      failure:(nullable void (^)(id data, NSURLResponse *response, NSError *error))failureHandler;

// Method to extract error message from server response.
// @param data: id, an object to be extracted.
// @param errorResponse: NSURLResponse, api response to be checked.
- (NSString *)errorMessageFromRequest:(id)data NSURLResponse:(NSURLResponse *)errorResponse;

#pragma mark - Public Methods

// Method returns acceptable HTTP status codes.
+ (NSIndexSet *)acceptableStatusCodes;

// Method to check URLResponse status code if acceptable.
// @param response: NSURLResponse, api response to be checked.
+ (BOOL)isStatusCodeAcceptable:(NSURLResponse *)response;

// Method to check internet reachability.
+ (BOOL)isInternetReachable;

@end

NS_ASSUME_NONNULL_END
