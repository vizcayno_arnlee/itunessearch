//
//  RequestController.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestController : NSObject

// Server URL, API server url.
@property (strong, nonatomic, readonly) NSString *server;

// Recursive lock for requests to be processed one at a time
@property (nonatomic, retain) NSRecursiveLock *requestLock;

// Initialize class.
- (id)init;

// Initialize class with server url.
// @param url: server url to be set.
- (id)initWithServerURL:(NSString *)url;

// Method to set server url.
// @param url: NSString, url to set as server url.
- (void)setServerURL:(NSString *)url;
// Method to get server url.
- (NSString *)getServerURL;
// Method to check server url is empty.
- (BOOL)isServerURLEmpty;

// Method to request API with POST method.
// @param action: NSString, API action to request or execute.
// @param parameters: NSDictionary, parameters for API request.
// @param completion: A block method to be executed when API request is finished successfully or failed.
// This block returns response data, API web response and error for any error encountered.
- (void)postRequestWithAction:(NSString *)action
                   parameters:(NSDictionary *)parameters
                   completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler;

@end

NS_ASSUME_NONNULL_END
