//
//  SQLDataManager.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SQLDataManager : NSObject

// Singleton class
+ (SQLDataManager *)sharedManager;

// NSManagedObjectContext, Managed object context for the application.
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
// NSManagedObjectModel, Managed object model for the application.
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
// NSPersistentStoreCoordinator, Persistent store coordinator for the application.
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// Method to save any changes in Managed object context
- (BOOL)saveContext;
// Method to get database directory.
// @return NSURL, database directory.
- (NSURL *)applicationDatabaseDirectory;

// directory, database folder directory
@property (nonatomic, strong) NSString *directory;

// Method to set database folder directory.
// @param directory: NSString, folder directory.
- (void)setDirectory:(NSString *)directory;
// Method to get database folder directory.
// @return NSString, database folder directory.
- (NSString *)getDatabaseDirectory;

@end

NS_ASSUME_NONNULL_END
