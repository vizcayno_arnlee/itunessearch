//
//  ImageManager.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "ImageManager.h"

@implementation ImageManager

#pragma mark - Public Methods

+ (void)imageFromURL:(NSString *)imageURL
          completion:(void (^)(UIImage *image, NSString *imageURL))completionHandler
{
    if (imageURL == NULL)
    {
        completionHandler(nil, imageURL);
        
        return;
    }
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
    {
            NSURL *url = [NSURL URLWithString:imageURL];
            NSData *data = [NSData dataWithContentsOfURL:url];
           
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
                UIImage *image = [UIImage imageWithData:data];
                
                completionHandler(image, imageURL);
            });
    });
}

@end
