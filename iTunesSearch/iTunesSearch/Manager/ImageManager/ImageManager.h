//
//  ImageManager.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageManager : NSObject

#pragma mark - Public Methods

// Method to fetch image from a URL
// @param imageURL: NSString, image url.
// @param completion: A block method to be executed when fetching of image from a URL is finished successfully or failed.
// This block returns image data and image url used to fetch.
+ (void)imageFromURL:(NSString *)imageURL
          completion:(void (^)(UIImage *image, NSString *imageURL))completionHandler;

@end
