//
//  MusicTableViewCell.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "MusicTableViewCell.h"

@implementation MusicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _explicitLabel.layer.cornerRadius = 4;
    _explicitLabel.layer.masksToBounds = true;
    
    [_explicitLabel setHidden:YES];
    [_artworkImageView setImage:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
