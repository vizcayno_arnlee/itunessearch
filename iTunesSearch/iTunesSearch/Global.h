//
//  Global.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

// Class contains all needed header import, organize import in one class
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "Utility.h"
#import "AppDelegate.h"

#import "RequestManager.h"
#import "SearchHistoryManager.h"
#import "ImageManager.h"

#import "TrackObject.h"

#import "MusicTableViewCell.h"

#import "DetailsViewController.h"


