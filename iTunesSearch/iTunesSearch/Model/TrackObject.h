//
//  TrackObject.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define kTrackID            @"trackID"
#define kTrackName          @"trackName"
#define kTrackViewUrl       @"trackViewUrl"
#define kTrackPrice         @"trackPrice"
#define kCurrency           @"currency"
#define kCollectionName     @"collectionName"
#define kCollectionViewUrl  @"collectionViewUrl"
#define kArtistName         @"artistName"
#define kArtistViewUrl      @"artistViewUrl"
#define kPrimaryGenreName   @"primaryGenreName"
#define kArtworkUrl30       @"artworkUrl30"
#define kArtworkUrl60       @"artworkUrl60"
#define kArtworkUrl100      @"artworkUrl100"
#define kKind               @"kind"
#define kReleaseDate        @"releaseDate"
#define kTrackExplicitness  @"trackExplicitness"

NS_ASSUME_NONNULL_BEGIN

@interface TrackObject : NSObject

@property (strong, nonatomic, readonly) NSString *trackID;

@property (strong, nonatomic, readonly) NSString *trackName;

@property (strong, nonatomic, readonly) NSString *trackViewUrl;

@property (strong, nonatomic, readonly) NSString *currency;

@property (strong, nonatomic, readonly) NSString *collectionName;

@property (strong, nonatomic, readonly) NSString *collectionViewUrl;

@property (strong, nonatomic, readonly) NSString *artistName;

@property (strong, nonatomic, readonly) NSString *artistViewUrl;

@property (strong, nonatomic, readonly) NSString *primaryGenreName;

@property (strong, nonatomic, readonly) NSString *artworkUrl30;

@property (strong, nonatomic, readonly) NSString *artworkUrl60;

@property (strong, nonatomic, readonly) NSString *artworkUrl100;

@property (strong, nonatomic, readonly) NSDate *releaseDate;

@property (strong, nonatomic, readonly) NSString *kind;

@property (readonly) CGFloat trackPrice;

@property (readonly) BOOL trackExplicitness;

// Track more information.
@property (strong, nonatomic) NSDictionary *moreTrackInformation;

- (id)initWithDictionary:(NSDictionary *)trackInfo;

@end

NS_ASSUME_NONNULL_END
