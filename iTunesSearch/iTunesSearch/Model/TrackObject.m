//
//  TrackObject.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "TrackObject.h"

@implementation TrackObject

- (id)initWithDictionary:(NSDictionary *)trackInfo
{
    self = [super init];
    if (self) {
        if ([trackInfo objectForKey:kTrackID]) _trackID = [trackInfo objectForKey:kTrackID];
        if ([trackInfo objectForKey:kTrackName]) _trackName = [trackInfo objectForKey:kTrackName];
        if ([trackInfo objectForKey:kTrackViewUrl]) _trackViewUrl = [trackInfo objectForKey:kTrackViewUrl];
        if ([trackInfo objectForKey:kTrackPrice]) _trackPrice = [[trackInfo objectForKey:kTrackPrice] floatValue];
        if ([trackInfo objectForKey:kCurrency]) _currency = [trackInfo objectForKey:kCurrency];
        if ([trackInfo objectForKey:kCollectionName]) _collectionName = [trackInfo objectForKey:kCollectionName];
        if ([trackInfo objectForKey:kCollectionViewUrl]) _collectionViewUrl = [trackInfo objectForKey:kCollectionViewUrl];
        if ([trackInfo objectForKey:kArtistName]) _artistName = [trackInfo objectForKey:kArtistName];
        if ([trackInfo objectForKey:kArtistViewUrl]) _artistViewUrl = [trackInfo objectForKey:kArtistViewUrl];
        if ([trackInfo objectForKey:kPrimaryGenreName]) _primaryGenreName = [trackInfo objectForKey:kPrimaryGenreName];
        if ([trackInfo objectForKey:kArtworkUrl30]) _artworkUrl30 = [trackInfo objectForKey:kArtworkUrl30];
        if ([trackInfo objectForKey:kArtworkUrl60]) _artworkUrl60 = [trackInfo objectForKey:kArtworkUrl60];
        if ([trackInfo objectForKey:kArtworkUrl100]) _artworkUrl100 = [trackInfo objectForKey:kArtworkUrl100];
        if ([trackInfo objectForKey:kKind]) _kind = [trackInfo objectForKey:kKind];
        if ([trackInfo objectForKey:kTrackExplicitness])
            _trackExplicitness = [[trackInfo objectForKey:kTrackExplicitness] isEqualToString:@"explicit"];
        if ([trackInfo objectForKey:kReleaseDate]) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
            _releaseDate = [formatter dateFromString:[trackInfo objectForKey:kReleaseDate]];
        }
        
        _moreTrackInformation = [[NSDictionary alloc] initWithDictionary:trackInfo];
    }
    return self;
}

@end
