//
//  Track.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "Track.h"

@implementation Track

@synthesize track_id;

@synthesize track_name;

@synthesize track_view_url;

@synthesize currency;

@synthesize collection_name;

@synthesize collection_view_url;

@synthesize artist_name;

@synthesize artist_view_url;

@synthesize primary_genre_name;

@synthesize artwork_url_30;

@synthesize artwork_url_60;

@synthesize release_date;

@synthesize track_price;

@synthesize track_explicitness;

@end
