//
//  Track.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Track : NSManagedObject

@property (strong, nonatomic) NSString *track_id;

@property (strong, nonatomic) NSString *track_name;

@property (strong, nonatomic) NSString *track_view_url;

@property (strong, nonatomic) NSString *currency;

@property (strong, nonatomic) NSString *collection_name;

@property (strong, nonatomic) NSString *collection_view_url;

@property (strong, nonatomic) NSString *artist_name;

@property (strong, nonatomic) NSString *artist_view_url;

@property (strong, nonatomic) NSString *primary_genre_name;

@property (strong, nonatomic) NSString *artwork_url_30;

@property (strong, nonatomic) NSString *artwork_url_60;

@property (strong, nonatomic) NSDate *release_date;

@property CGFloat track_price;

@property BOOL track_explicitness;

@end

NS_ASSUME_NONNULL_END
