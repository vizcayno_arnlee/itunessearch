//
//  SearchViewController.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "SearchViewController.h"

static NSString *RecentSearchCellIdentifier = @"historySearchCell";
static NSString *MusicSearchCellIdentifier = @"musicSearchCell";

@interface SearchViewController () <UISearchBarDelegate> {
    RequestManager *requestManager;
    
    NSString *countryCode;
    
    NSMutableArray *historySearchArray;
    NSMutableArray *resultSearchArray;
    
    ViewType viewType;
    MediaType mediaType;
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    requestManager = [RequestManager sharedManager];
    [requestManager setServerURL:kServerURL];
    
    countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    
    historySearchArray = [[NSMutableArray alloc] init];
    resultSearchArray = [[NSMutableArray alloc] init];
    
    viewType = kSearchHistoryViewType;
    mediaType = kMusicMediaType;
    
    [historySearchArray addObjectsFromArray:[SearchHistoryManager getSearchHistoryArray]];

    [self.tableView setTableHeaderView:_searchView];
}

#pragma mark - Methods

- (void)updateSearchView {
    [_searchBar resignFirstResponder];
    
    if ([_searchBar.text length] > 0) {
        viewType = kSearchResultViewType;
        
        [SearchHistoryManager addSearchHistory:_searchBar.text];
        
        [historySearchArray removeAllObjects];
        [historySearchArray addObjectsFromArray:[SearchHistoryManager getSearchHistoryArray]];
        
        [self requestData];
    } else {
        viewType = kSearchHistoryViewType;
        
        [resultSearchArray removeAllObjects];
        
        [self.tableView reloadData];
    }
}

- (void)requestData {
    NSString *mediaTypeString = @"music";
    if (mediaType == kMovieMediaType)
        mediaTypeString = @"movie";
    else if (mediaType == kPodcastMediaType)
        mediaTypeString = @"podcast";
    
    [Utility showActivityIndicator];
    [requestManager postRequestWithAction:kSearchAction
                               parameters:@{@"term":_searchBar.text, @"media":mediaTypeString, @"country":countryCode}
                               completion:^(NSDictionary *data, NSURLResponse * response, NSError * error) {
                                   NSLog(@"completion: %@", data);
                                   [Utility hideActivityIndicator];
                                   
                                   [self->resultSearchArray removeAllObjects];
                                   [self->resultSearchArray addObjectsFromArray:[data objectForKey:kResults]];
                                   
                                   [self.tableView reloadData];
                               }
                                  failure:^(id data, NSURLResponse * response, NSError * error) {
                                      // NSLog(@"failure: %@", data);
                                      [Utility hideActivityIndicator];
                                   
                                      NSString *msg = [self->requestManager errorMessageFromRequest:data NSURLResponse:response];
                                      [Utility showAlertTitle:kError withMessage:msg viewController:self];
                                      
                                      [self.tableView reloadData];
                                  }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (viewType == kSearchResultViewType)
        return @"Search Result";
    else if (viewType == kSearchHistoryViewType)
        return @"Search History";
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (viewType == kSearchResultViewType)
        return [resultSearchArray count];
    else if (viewType == kSearchHistoryViewType)
        return [historySearchArray count];
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (viewType == kSearchResultViewType) {
        return 70;
    } else if (viewType == kSearchHistoryViewType) {
        return 44;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MusicTableViewCell *cell;
    
    if (viewType == kSearchResultViewType) {
        cell = [tableView dequeueReusableCellWithIdentifier:MusicSearchCellIdentifier forIndexPath:indexPath];
        
        TrackObject *track = [[TrackObject alloc] initWithDictionary:[resultSearchArray objectAtIndex:indexPath.row]];
        
        [cell.trackNameLabel setText:[track trackName]];
        [cell.genreLabel setText:[track primaryGenreName]];
        [cell.artistLabel setText:[track artistName]];
        [cell.explicitLabel setHidden:[track trackExplicitness]];
        
        if ([track trackPrice] > 0) {
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[track currency]];
            NSString *currencySymbol = [NSString stringWithFormat:@"%@",[locale displayNameForKey:NSLocaleCurrencySymbol value:[track currency]]];
            [cell.priceLabel setText:[NSString stringWithFormat:@"%@%0.2f", currencySymbol, [track trackPrice]]];
        } else {
            [cell.priceLabel setText:@"FREE"];
        }
        
        [cell.artworkImageView setImage:[UIImage imageNamed:@"image_placeholder"]];
        [ImageManager imageFromURL:[track artworkUrl60] completion:^(UIImage *image, NSString *imageURL) {
            if ([[track artworkUrl60] isEqualToString:imageURL])
                [cell.artworkImageView setImage:image];
         }];
        
    } else if (viewType == kSearchHistoryViewType) {
        cell = [tableView dequeueReusableCellWithIdentifier:RecentSearchCellIdentifier forIndexPath:indexPath];
        
        NSString *historySearchText = (NSString *)[historySearchArray objectAtIndex:indexPath.row];
        
        [cell.textLabel setText:historySearchText];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (viewType == kSearchResultViewType) {
        
    } else if (viewType == kSearchHistoryViewType) {
        viewType = kSearchResultViewType;
        
        NSString *historySearchText = (NSString *)[historySearchArray objectAtIndex:indexPath.row];
        [_searchBar setText:historySearchText];
        
        [self updateSearchView];
    }
}

#pragma mark - Search bar delegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
    
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self updateSearchView];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self updateSearchView];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"pushToDetails"]) {
        MusicTableViewCell *cell = (MusicTableViewCell *)sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        DetailsViewController *viewController = (DetailsViewController *)[segue destinationViewController];
        viewController.trackInformation = [[TrackObject alloc] initWithDictionary:[resultSearchArray objectAtIndex:indexPath.row]];
    }
}

- (IBAction)onMediaType:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            mediaType = kMusicMediaType;
            break;
        case 1:
            mediaType = kMovieMediaType;
            break;
        case 2:
            mediaType = kPodcastMediaType;
            break;
    }
    
    [self updateSearchView];
}
@end
