//
//  DetailsViewController.m
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "DetailsViewController.h"

static NSString *DetailsCellIdentifier = @"detailsCell";

@interface DetailsViewController () {
    NSDateFormatter *formatter;
}

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMMM yyyy"];
    
    _explicitLabel.layer.cornerRadius = 4;
    _explicitLabel.layer.masksToBounds = true;
    [_explicitLabel setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_artworkImageView setImage:[UIImage imageNamed:@"image_placeholder"]];
    [ImageManager imageFromURL:[_trackInformation artworkUrl60] completion:^(UIImage *image, NSString *imageURL) {
        [self->_artworkImageView setImage:image];
    }];
    
    [_trackNameLabel setText:[NSString stringWithFormat:@"%@ ⇗", [_trackInformation trackName]]];
    [_explicitLabel setHidden:[_trackInformation trackExplicitness]];
    
    if ([_trackInformation trackPrice] > 0) {
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[_trackInformation currency]];
        NSString *currencySymbol = [NSString stringWithFormat:@"%@",[locale displayNameForKey:NSLocaleCurrencySymbol value:[_trackInformation currency]]];
        [_priceLabel setText:[NSString stringWithFormat:@"%@%0.2f", currencySymbol, [_trackInformation trackPrice]]];
    } else {
        [_priceLabel setText:@"FREE"];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [SearchHistoryManager addVisitedHistory:[_trackInformation moreTrackInformation]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DetailsCellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row > 1)
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    else
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    
    NSString *text = @"";
    
    switch (indexPath.row) {
        case 0:
            text = [NSString stringWithFormat:@"Genre: %@",[_trackInformation primaryGenreName]];
            break;
        case 1:
            text = [NSString stringWithFormat:@"Released %@", [formatter stringFromDate:[_trackInformation releaseDate]]];
            break;
        case 2:
            text = [_trackInformation artistName];
            break;
        case 3:
            text = [_trackInformation collectionName];
            break;
        default:
            break;
    }
    
    if ([text length] == 0)
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    [cell.textLabel setText:text];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 1) {
        NSURL *url = nil;
        
        if (indexPath.row == 2 && [[_trackInformation artistName] length] > 0)
            url = [NSURL URLWithString:[_trackInformation artistViewUrl]];
        else if (indexPath.row == 3 && [[_trackInformation collectionName] length] > 0)
            url = [NSURL URLWithString:[_trackInformation collectionViewUrl]];
        
        if (url)
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}

- (IBAction)onTrack:(id)sender {
    NSURL *url = [NSURL URLWithString:[_trackInformation trackViewUrl]];
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
}
@end
