//
//  DetailsViewController.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "Global.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailsViewController : UIViewController

@property (strong, nonatomic) TrackObject *trackInformation;

@property (weak, nonatomic) IBOutlet UIImageView *artworkImageView;

@property (weak, nonatomic) IBOutlet UILabel *trackNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *explicitLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)onTrack:(id)sender;

@end

NS_ASSUME_NONNULL_END
