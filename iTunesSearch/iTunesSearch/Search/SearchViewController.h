//
//  SearchViewController.h
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 30/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

#import "Global.h"

#define kServerURL      @"https://itunes.apple.com/"
#define kSearchAction   @"search"
#define kResults        @"results"

typedef enum ViewType : NSUInteger {
    kUnknownViewType = 0,
    kSearchHistoryViewType,
    kSearchResultViewType
} ViewType;

typedef enum MediaType : NSUInteger {
    kUnknownMediaType = 0,
    kMusicMediaType,
    kMovieMediaType,
    kPodcastMediaType
} MediaType;

NS_ASSUME_NONNULL_BEGIN

@interface SearchViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)onMediaType:(id)sender;

@end

NS_ASSUME_NONNULL_END
