//
//  VisitedViewController.swift
//  iTunesSearch
//
//  Created by Arnlee Vizcayno on 31/03/2019.
//  Copyright © 2019 Arnlee Vizcayno. All rights reserved.
//

import UIKit

class VisitedViewController: UITableViewController {

    let VisitedHistoryCellIdentifier = "visitedHistoryCell"
    
    var visitedHistoryArray : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        visitedHistoryArray.removeAllObjects()
        visitedHistoryArray.addObjects(from: SearchHistoryManager.getVisitedHistoryArray())
        
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Visited History";
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return visitedHistoryArray.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MusicTableViewCell = tableView.dequeueReusableCell(withIdentifier: VisitedHistoryCellIdentifier, for: indexPath) as! MusicTableViewCell

        let track = TrackObject.init(dictionary: visitedHistoryArray.object(at: indexPath.row) as! [AnyHashable : Any])

        cell.trackNameLabel?.text = track.trackName
        cell.genreLabel?.text = String(format: "%@/%@", track.kind.uppercased(), track.primaryGenreName)
        cell.artistLabel?.text = track.artistName
        cell.explicitLabel?.isHidden = track.trackExplicitness
        
        if track.trackPrice > 0 {
            let locale = NSLocale.init(localeIdentifier: track.currency)
            let currencySymbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value: track.currency)
            cell.priceLabel?.text = String(format: "%@%0.2f", currencySymbol ?? "", track.trackPrice) 
        } else {
            cell.priceLabel?.text = "FREE"
        }
        
        cell.artworkImageView?.image = UIImage.init(named: "image_placeholder")
        ImageManager.image(fromURL: track.artworkUrl60) { (image, imageURL) in
            if track.artworkUrl60 == imageURL {
                cell.artworkImageView?.image = image
            }
        }
        
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushToDetails" {
            let cell : MusicTableViewCell = sender as! MusicTableViewCell
            let indexPath : NSIndexPath = self.tableView.indexPath(for: cell)! as NSIndexPath
            
            let viewController : DetailsViewController = segue.destination as! DetailsViewController
            viewController.trackInformation = TrackObject.init(dictionary: visitedHistoryArray.object(at: indexPath.row) as! [AnyHashable : Any])
        }
    }
}
