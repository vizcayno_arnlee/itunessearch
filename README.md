# README #

### iTunes Search Application ###
IDE: Xcode 10
Laguage: Objective-C and Swift - Swift to show language bridging.  
Achitecture: Model View Controller(MVC) - for simple project just making it plain, manageable and simple. 

### Search Createria ###
 - Term, search text from user input
 - Country, used device country code
 - Media, options for user choose music, movie and podcase
 
### Search History (Search Tab Persistence) ###
 - Display list of maximum 6 search text input previously by user. Oldest deleted and new inserted at first index(FIFO).  
 - Used NSUserDefaults instead of CoreData since data is simple and straight forward. Option to use CodeData is ready to use.

### Search Result (Search Tab) ###
 - Display list of track reponse from API.
 - Attribute displayed are track name, explicitness, genre, artist and price.

### Visited History (History Tab Persistence) ###
- Display list of maximum 10 track recently visited by user. Oldest deleted and new inserted at first index(FIFO). 
 - Attribute displayed are track name, explicitness, kind/genre, artist and price.
  - Used NSUserDefaults instead of CoreData since data is simple and straight forward. Option to use CodeData is ready to use.

### Details View ###
- Display more details about the track.
- Attribute displayed are track name, explicitness, genre, artist, collection, price and release date.
- Tap track name will redirect to track url view.
- Tap artist name will redirect to artist url view.
- Tap collection name will redirect to collection url view.

### How do to run? ###
- Project is build in Xcode version 10.0
- Deployment target iOS 11.0
- Device target is iPhone
- Orientation supported is portrait and upside down
- Build and Run in Simulator iPhone XR

